/**
 *  Copyright (c) 2015, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 */

import {
  GraphQLBoolean,
  GraphQLFloat,
  GraphQLID,
  GraphQLInt,
  GraphQLList,
  GraphQLNonNull,
  GraphQLObjectType,
  GraphQLSchema,
  GraphQLString,
} from 'graphql';

import {
  connectionArgs,
  connectionDefinitions,
  connectionFromArray,
  fromGlobalId,
  globalIdField,
  mutationWithClientMutationId,
  nodeDefinitions,
} from 'graphql-relay';

import {
  // Import methods that your schema can use to interact with your database
  User,
  Todo,
  getUser,
  getViewer,
  getTodos,
  getTodo,
  editTodo,
  addTodo,
  removeTodo,
} from './database';

/**
 * We get the node interface and field from the Relay library.
 *
 * The first method defines the way we resolve an ID to its object.
 * The second defines the way we resolve an object to its GraphQL type.
 */
var {nodeInterface, nodeField} = nodeDefinitions(
  (globalId) => {
    var {type, id} = fromGlobalId(globalId);
    if (type === 'User') {
      return getUser(id);
    } else if (type === 'Todo') {
        return getTodo();
    } else {
      return null;
    }
  },
  (obj) => {
    if (obj instanceof User) {
      return userType;
    } else if (obj instanceof Todo)  {
      return todoType;
    } else {
      return null;
    }
  }
);

/**
 * Define your own types here
 */

var userType = new GraphQLObjectType({
  name: 'User',
  description: 'A person who uses our app',
  fields: () => ({
    id: globalIdField('User'),
    todos: {
      type: todoConnection,
      description: 'A person\'s todos list',
      args: connectionArgs,
      resolve: (_,args) => connectionFromArray(getTodos(), args),
    },
    count: {
      type: GraphQLInt,
      description: 'Counter for todos',
      resolve: () => getTodos().length,
    },
    picture: {
      type: GraphQLString,
      description: 'User url picture',
    }
  }),
  interfaces: [nodeInterface],
});

var todoType = new GraphQLObjectType({
  name: 'Todo',
  description: 'A text descript of a todo task',
  fields: () => ({
    id: globalIdField('Todo'),
    description: {
      type: GraphQLString,
      description: 'The todo description',
    },
    created_date: {
      type: GraphQLString,
      description: 'The datetime when the todo item was created',
    },
  }),
  interfaces: [nodeInterface],
});

/**
 * Define your own connection types here
 */
var {connectionType: todoConnection, edgeType: todoEdgeType} =
  connectionDefinitions({name: 'Todo', nodeType: todoType});

/**
 * This is the type that will be the root of our query,
 * and the entry point into our schema.
 */
var queryType = new GraphQLObjectType({
  name: 'Query',
  fields: () => ({
    node: nodeField,
    // Add your own root fields here
    viewer: {
      type: userType,
      resolve: () => getViewer(),
    },
  }),
});

/**
 * This is the type that will be the root of our mutations,
 * and the entry point into performing writes in our schema.
 */

const removeTodoMutationType = mutationWithClientMutationId({
  name: 'RemoveTodo',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
  },
  outputFields: {
    deletedTodoId: {
      type: GraphQLID,
      resolve: ({id}) => id,
    },
    viewer: {
      type: userType,
      resolve: () => getViewer(),
    },
  },
  mutateAndGetPayload: ({id}) => {
    const localTodoId = fromGlobalId(id).id;
    removeTodo(localTodoId);
    return {id};
  },
});

const addTodoMutationType = mutationWithClientMutationId({
  name: 'AddTodo',
  inputFields: {
    description: { type: new GraphQLNonNull(GraphQLString) },
  },
  outputFields: {
    todoEdge: {
      type: todoEdgeType,
      resolve: ({localTodoId}) => {
        const todo = getTodo(localTodoId);
        return {
          node: todo,
        };
      },
    },
    viewer: {
      type: userType,
      resolve: () => getViewer(),
    },
  },
  mutateAndGetPayload: ({description}) => {
    const localTodoId = addTodo(description);
    return {localTodoId};
  },
});

const editTodoMutationType = mutationWithClientMutationId({
  name: 'EditTodo',
  inputFields: {
    id: { type: new GraphQLNonNull(GraphQLID) },
    description: { type: new GraphQLNonNull(GraphQLID) },
  },
  outputFields: {
    todo: {
      type: todoType,
      resolve: ({localTodoId}) => getTodo(localTodoId),
    },
  },
  mutateAndGetPayload: ({id, description}) => {
    const localTodoId = fromGlobalId(id).id;
    editTodo(localTodoId, description);
    return {localTodoId};
  },
});

var mutationType = new GraphQLObjectType({
  name: 'Mutation',
  fields: () => ({
    removeTodo: removeTodoMutationType,
    addTodo: addTodoMutationType,
    editTodo: editTodoMutationType,
  })
});

/**
 * Finally, we construct our schema (whose starting query type is the query
 * type we defined above) and export it.
 */
export var Schema = new GraphQLSchema({
  query: queryType,
  // Uncomment the following after adding some mutation fields:
  mutation: mutationType
});
