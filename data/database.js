/**
 *  Copyright (c) 2015, Facebook, Inc.
 *  All rights reserved.
 *
 *  This source code is licensed under the BSD-style license found in the
 *  LICENSE file in the root directory of this source tree. An additional grant
 *  of patent rights can be found in the PATENTS file in the same directory.
 */

 import moment from 'moment';
// Model types
class User {}
class Todo {}

// Mock data
let nextTodoId = 0;
var todoList = ['First task', 'Second task', 'Third task'].map((description, i) => {
  var todo = new Todo();
  todo.description = description;
  todo.id = `${i}`;
  todo.created_date = moment().format('DD/MM/YYYY HH:mm');
  nextTodoId = i + 1;
  return todo;
});

var viewer = new User();
viewer.id = '1';
viewer.picture = 'https://tribalingua.files.wordpress.com/2013/06/mateus.png';
viewer.count = todoList.length;

module.exports = {
  // Export methods that your schema can use to interact with your database
  getUser: (id) => id === viewer.id ? viewer : null,
  getViewer: () => viewer,
  getTodos: () => todoList,
  getTodo: (id) => todoList.find(todo => todo.id === id),
  editTodo,
  addTodo,
  removeTodo,
  User,
  Todo
};

function editTodo (id, description) {
  (todoList.find(t => t.id === id) || {}).description = description;
}

function removeTodo (id) {
  todoList = todoList.filter(todo => todo.id !== id);
}

function addTodo(description) {
  var todo = new Todo();
  todo.description = description;
  todo.id = `${nextTodoId++}`;
  todo.created_date = moment().format('DD/MM/YYYY HH:mm');
  todoList.push(todo);
  return todo.id;
}
