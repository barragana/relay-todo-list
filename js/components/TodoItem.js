import React from 'react';
import Relay from 'react-relay';

import RemoveTodoMutation from '../mutations/RemoveTodoMutation';
import EditTodoMutation from '../mutations/EditTodoMutation';

import TodoDescriptionInput from './TodoDescriptionInput';

import './todoItem.scss';

class TodoItem extends React.Component {

  state = {
    isEditing: false,
  }

  saveHandler = (description) => {
    this.setState({isEditing: !this.state.isEditing});

    this.props.relay.commitUpdate(
      new EditTodoMutation({description, todo: this.props.todo})
    );
  }

  deleteHandler = () => {
    this.setState({isEditing: !this.state.isEditing});
    
    this.props.relay.commitUpdate(
      new RemoveTodoMutation({todo: this.props.todo, viewer: this.props.viewer})
    );
  }

  editHandler = () => {
    this.setState({isEditing: !this.state.isEditing});
  }

  render() {
    return (
      <div className="todo-item">
        <span className="picture">
          <img src={this.props.viewer.picture} height="50" width="50" />
        </span>
        <div className="input">
          <div className="arrow" />
          {this.state.isEditing ?
            <TodoDescriptionInput
              className="todo"
              initialValue={this.props.todo.description}
              isEditing={this.state.isEditing}
              onSave={this.saveHandler}
              onCancel={this.editHandler}
              onDelete={this.deleteHandler}
            /> :
            <input
              className="todo"
              value={this.props.todo.description}
              readOnly />
          }
          <span className="create-date">{this.props.todo.created_date}</span>
        </div>
        <i className="fa fa-pencil fa-2x" onClick={this.editHandler} />
        <i className="fa fa-trash fa-2x" onClick={this.deleteHandler} />
      </div>
    );
  }
}

export default Relay.createContainer(TodoItem, {
  fragments: {
    todo: () => Relay.QL`
      fragment on Todo {
        id,
        description,
        created_date,
        ${EditTodoMutation.getFragment('todo')},
        ${RemoveTodoMutation.getFragment('todo')},
      },
    `,
    viewer: () => Relay.QL`
      fragment on User {
        picture,
        ${RemoveTodoMutation.getFragment('viewer')},
      }
    `,
  },
});
