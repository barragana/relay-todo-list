import React from 'react';
import ReactDOM from 'react-dom';

const ENTER_KEY_CODE = 13;
const ESC_KEY_CODE = 27;

export default class TodoDescriptionInput extends React.Component {

  state = {
    description: this.props.initialValue || '',
  };

  componentDidMount () {
    if (this.props.isEditing) {
      ReactDOM.findDOMNode(this).focus();
    }
  }

  changeHandler = (e) => {
    this.setState({description: e.target.value});
  }

  cancelHandler = () => {
    if (this.props.onCancel && this.props.isEditing) {
      this.setState({description: this.props.initialValue});
      this.props.onCancel();
    }
  }

  keyDownHandler = (e) => {

    if (e.keyCode !== ESC_KEY_CODE && e.keyCode !== ENTER_KEY_CODE) {
      return;
    }

    if (e.keyCode === ESC_KEY_CODE) {
      return this.cancelHandler();
    }

    if (e.keyCode === ENTER_KEY_CODE && this.state.description === this.props.initialValue) {
      return this.cancelHandler();
    }

    if (e.keyCode === ENTER_KEY_CODE && this.state.description.trim() === '' && this.props.onDelete) {
      return this.props.onDelete();
    }

    this.props.onSave(this.state.description);
    ReactDOM.findDOMNode(this).blur();
    this.setState({description: ''});
  }

  render() {
    return (
      <input
        type="text"
        className={this.props.className}
        onBlur={this.cancelHandler}
        onChange={this.changeHandler}
        onKeyDown={this.keyDownHandler}
        placeholder={this.props.placeholder}
        value={this.state.description}
      />
    );
  }
}
