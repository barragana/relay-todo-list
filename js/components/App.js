import React from 'react';
import Relay from 'react-relay';

import AddTodoMutation from '../mutations/AddTodoMutation';

import TodoItem from './TodoItem';
import TodoDescriptionInput from './TodoDescriptionInput';

import './app.scss';

class App extends React.Component {

  saveHandler = (description) => {
    this.props.relay.commitUpdate(
      new AddTodoMutation({description, viewer: this.props.viewer})
    );
  }

  render() {
    return (
      <div className="app">
        <div className="top">
          <span className="count">{this.props.viewer.count} item(s)</span>
        </div>
        <div className="middle">
          <div className="wrapper">
            {this.props.viewer.todos.edges.map(edge =>
              <TodoItem todo={edge.node} key={edge.node.id} viewer={this.props.viewer} />
            )}
          </div>
        </div>
        <div className="bottom">
          <div className="bottom-item">
            <TodoDescriptionInput
              className="new-todo"
              onSave={this.saveHandler}
              placeholder="Enter your message"
              isEditing={true}
            />
          </div>
        </div>
      </div>
    );
  }
}

export default Relay.createContainer(App, {
  fragments: {
    viewer: () => Relay.QL`
      fragment on User {
        count,
        todos(first: 10) {
          edges {
            node {
              id,
              ${TodoItem.getFragment('todo')}
            }
          }
        },
        ${AddTodoMutation.getFragment('viewer')},
        ${TodoItem.getFragment('viewer')},
      }
    `,
  },
});
