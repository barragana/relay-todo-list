import Relay from 'react-relay';

export default class EditTodoMutation extends Relay.Mutation {
  static fragments = {
    todo: () => Relay.QL`
      fragment on Todo {
        id,
      }
    `,
  };

  getMutation() {
    return Relay.QL`mutation{editTodo}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on EditTodoPayload {
        todo {
          description,
        },
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'FIELDS_CHANGE',
      fieldIDs: {
        todo: this.props.todo.id,
      },
    }];
  }

  getVariables() {
    return {
      id: this.props.todo.id,
      description: this.props.description,
    };
  }

  getOptimisticResponse() {
    return {
      todo: {
        id: this.props.todo.id,
        description: this.props.description,
      },
    };
  }
}
