import Relay from 'react-relay';

export default class AddTodoMutation extends Relay.Mutation {
  static fragments = {
    viewer: () => Relay.QL`
      fragment on User {
        id,
        count,
      }
    `,
  };

  getMutation() {
    return Relay.QL`mutation{addTodo}`;
  }

  getFatQuery() {
    return Relay.QL`
      fragment on AddTodoPayload {
        todoEdge,
        viewer {
          todos,
          count,
        },
      }
    `;
  }

  getConfigs() {
    return [{
      type: 'RANGE_ADD',
      parentName: 'viewer',
      parentID: this.props.viewer.id,
      connectionName: 'Todo',
      edgeName: 'todoEdge',
      rangeBehaviors: () => 'append',
    }];
  }

  getVariables() {
    return {
      description: this.props.description,
    };
  }

  getOptimisticResponse() {
    return {
      todoEdge: {
        node: {
          complete: false,
          description: this.props.description,
        },
      },
      viewer: {
        id: this.props.viewer.id,
        count: this.props.viewer.count + 1,
      },
    };
  }
}
